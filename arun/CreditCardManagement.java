import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;


public class CreditCardManagement {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String filePath = "Z:/Documents/creditcard.txt";
		HashMap<Integer, Map> finalMap = getCardDetails(filePath);
		System.out.println(finalMap);
	}

	private static HashMap<Integer, Map> getCardDetails(String filePath) {
		File inputFile = new File(filePath);
		HashMap<Integer, Map> finalMap = new HashMap<>();
		if(inputFile.exists())
		{
			try 
			{
				FileReader fileReader  = new FileReader(inputFile);
				BufferedReader buffReader = new BufferedReader(fileReader);
				String fileData = null;
				List<CreditCardVO> customerList = new ArrayList<>();
				Map<String, CreditCardVO> visaMap = new HashMap<>();
				Map<String, CreditCardVO> amexMap = new HashMap<>();
				
				
				while((fileData = buffReader.readLine()) != null)
				{
					String[] records = fileData.split("\\|");
					CreditCardVO detailsVO = new CreditCardVO();
					validateRecord(records, detailsVO);
					detailsVO.setBillAmount(Integer.parseInt(records[2]));
					customerList.add(detailsVO);		
				}
				
				//remove duplicate records
				List<CreditCardVO> nonDupList = new ArrayList<>();	
				for (int i = 0; i < customerList.size(); i++) 
				{
					boolean isDup = false;
					for (int j = i+1; j < customerList.size(); j++)
					{
						if((customerList.get(i).equals(customerList.get(j))))
						{
							isDup = true;
						}
					}
					
					if(!isDup)
					{
						nonDupList.add(customerList.get(i));
					}
				}

				//identify unique card number
				Set<String> uniqueCardNumber = new HashSet<>();
				for (int i = 0; i < nonDupList.size(); i++) 
				{
					uniqueCardNumber.add(nonDupList.get(i).getCreditCardNumber());
				}
				
				//calculate fine amount
				for (int i = 0; i < nonDupList.size(); i++) 
				{
					calculateFine(nonDupList.get(i));
				}

				//identify latest payment date based on card number
				List<Date> latestDate = new ArrayList<>();
				for (String cardNumber : uniqueCardNumber) 
				{
					for (int i = 0; i < nonDupList.size(); i++) 
					{
						if(cardNumber.equals(nonDupList.get(i).getCreditCardNumber()))
						{
							latestDate.add(nonDupList.get(i).getPaymentDate());
						}
					}
					
					Date latstPayDate = Collections.max(latestDate);	
					latestDate.clear();
					
					for (int i = 0; i < nonDupList.size(); i++) 
					{
						if(latstPayDate.equals(nonDupList.get(i).getPaymentDate()) && cardNumber.equals(nonDupList.get(i).getCreditCardNumber())) 
						{
							if(nonDupList.get(i).getCardType().equalsIgnoreCase("VISA"))
							{		
								visaMap.put(nonDupList.get(i).getCreditCardNumber(), nonDupList.get(i));
							}
							else
							{
								amexMap.put(nonDupList.get(i).getCreditCardNumber(), nonDupList.get(i));
							}
						}
					}
				}
				
				finalMap.put(1, visaMap);
				finalMap.put(2, amexMap);		
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			} 
			catch (IOException e)
			{
				e.printStackTrace();
			} 
			catch (CreditCardException e) 
			{
				e.printStackTrace();
			}

		}
		
		return finalMap;
	}

	private static void calculateFine(CreditCardVO detailsVO)
	{
		if(detailsVO.getPaymentDate().after(detailsVO.getDueDate()))
		{
			detailsVO.setGrade("B");
			long diff = detailsVO.getPaymentDate().getTime() - detailsVO.getDueDate().getTime();
			long days = diff / (1000*60*60*24);

			if(detailsVO.getCardType().equalsIgnoreCase("VISA"))
			{
				if(days <= 5)
				{
					int fine = (detailsVO.getBillAmount() * 10) / 100;
					detailsVO.setFine(fine);
				}
				else
				{
					int fine = (detailsVO.getBillAmount() * 20) / 100;
					detailsVO.setFine(fine);
				}
				
			}
			else
			{
				if(days <= 5)
				{
					int fine = (detailsVO.getBillAmount() * 10) / 100;
					detailsVO.setFine(fine);
				}
				else
				{
					if(detailsVO.getBillAmount() < 15000)
					{
						int fine = (detailsVO.getBillAmount() * 20) / 100;
						detailsVO.setFine(fine);
					}
					else
					{
						int fine = (detailsVO.getBillAmount() * 30) / 100;
						detailsVO.setFine(fine);
					}
				}
				
			}
		}
		else
		{
			detailsVO.setGrade("A");
			int fine = 0;
			detailsVO.setFine(fine);
		}
	}

	private static void validateRecord(final String[] records, CreditCardVO detailsVO) throws CreditCardException
	{
		boolean isInValid = false;
		if(records.length == 5)
		{
			if(!(records[0].trim().length() > 0))
			{
				isInValid = true;
				throw new CreditCardException("Card Number should not be empty");
			}
			if(!(records[1].trim().length() > 0))
			{
				isInValid = true;
				throw new CreditCardException("Name should not be empty");
			}
			if(!(records[2].trim().length() > 0))
			{
				isInValid = true;
				throw new CreditCardException("Bill Amount should not be empty");
			}
			if(!(records[3].trim().length() > 0))
			{
				isInValid = true;
				throw new CreditCardException("Due Date should not be empty");
			}
			if(!(records[4].trim().length() > 0))
			{
				isInValid = true;
				throw new CreditCardException("Payment should not be empty");
			}
		}
		else
		{
			throw new CreditCardException("All fields are Mandatory");
		}
		
		if(!isInValid)
		{
			if(records[0].startsWith("4"))
			{
				if(records[0].length() == 16)
				{
					detailsVO.setCreditCardNumber(records[0]);
					detailsVO.setCardType("VISA");
				}
				else
				{
					throw new CreditCardException("Invalid card number");
				}
			}
			else if(records[0].startsWith("34") || records[0].startsWith("37"))
			{
				if(records[0].length() == 15)
				{
					detailsVO.setCreditCardNumber(records[0]);
					detailsVO.setCardType("AMEX");
				}
				else
				{
					throw new CreditCardException("Invalid card number");
				}
			}
			
			if(Pattern.matches("^[a-zA-Z]*$", records[1]))
			{
				detailsVO.setName(records[1]);
			}
			else
			{
				throw new CreditCardException("Name mismatch");
			}
			
			if(records[3] != null && records[4] != null)
			{
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				try 
				{
					
					final Date dueDate = sdf.parse(records[3]);
					final Date paymentDate = sdf.parse(records[4]);
					detailsVO.setDueDate(dueDate);
					detailsVO.setPaymentDate(paymentDate);
				} 
				catch (ParseException e)
				{
					e.printStackTrace();
				}
				
			}
			
		}
		
	}

}

class CreditCardVO
{
	private String CreditCardNumber;
	private String Name;
	private int BillAmount;
	private Date DueDate;
	private Date PaymentDate;
	private int Fine;
	private String CardType;
	private String Grade;
	public String getCreditCardNumber() {
		return CreditCardNumber;
	}
	public void setCreditCardNumber(String creditCardNumber) {
		CreditCardNumber = creditCardNumber;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public int getBillAmount() {
		return BillAmount;
	}
	public void setBillAmount(int billAmount) {
		BillAmount = billAmount;
	}
	public Date getDueDate() {
		return DueDate;
	}
	public void setDueDate(Date dueDate) {
		DueDate = dueDate;
	}
	public Date getPaymentDate() {
		return PaymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		PaymentDate = paymentDate;
	}
	public int getFine() {
		return Fine;
	}
	public void setFine(int fine) {
		Fine = fine;
	}
	public String getCardType() {
		return CardType;
	}
	public void setCardType(String cardType) {
		CardType = cardType;
	}
	public String getGrade() {
		return Grade;
	}
	public void setGrade(String grade) {
		Grade = grade;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CreditCardVO [CreditCardNumber=" + CreditCardNumber + ", Name=" + Name + ", BillAmount=" + BillAmount
				+ ", DueDate=" + DueDate + ", PaymentDate=" + PaymentDate + ", Fine=" + Fine + ", CardType=" + CardType
				+ ", Grade=" + Grade + "]";
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		boolean isEqual = false;
		CreditCardVO other = (CreditCardVO) obj;
		if( (this.getCreditCardNumber().equals(other.getCreditCardNumber())) && (this.getName().equals(other.getName())) &&  (this.getBillAmount() == other.getBillAmount()) && (this.getDueDate().equals(other.getDueDate())) && (this.getPaymentDate().equals(other.getPaymentDate())))
		{
			isEqual = true;
		}
		return isEqual;
	}
	
}


class CreditCardException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CreditCardException(String message)
	{
		super(message);
	}
	public CreditCardException(Throwable message)
	{
		super(message);
	}
	
}