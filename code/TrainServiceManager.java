package com.domain.demo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;

public class TrainServiceManager {
	
	private static String travelDate="20-12-2016";
	private static String SPLITTER="|";
	
	//comment main method after done completion
	//all function should be public
	
	public static void main(String[] args)
	{
		String fileLocation ="D://corejava/trainData.txt";
		int source=1; int destination =7;
		
		try
		{
			getTrainDetails(fileLocation, source, destination, travelDate);
			getTrainSchedule(fileLocation);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	/* Return the list of trains for the given parameter */
	public static List<TrainDetailsVO> getTrainDetails(final String filePath, int source,
			int destination, String dateOfTravel)
			throws TrainServiceException {
		
		List<TrainDetailsVO> trainvo = new ArrayList<TrainDetailsVO>();
		try
		{
			trainvo = readFile(filePath);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("Train Details =>"+trainvo.toString());
		return trainvo;
	}

	 private static List<TrainDetailsVO> readFile(String filePath) {
		 List<TrainDetailsVO> tvo = new ArrayList<TrainDetailsVO>();
		 try
		 {
			 BufferedReader br = new BufferedReader(new FileReader("D://corejava/trainData.txt"));
			 String line = null;
			 
			 while((line=br.readLine())!=null)
			 {
				 tvo.add(covert(line));
			 }
		 }catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		return tvo;
	}


	private static TrainDetailsVO covert(String line) throws TrainServiceException, ParseException {
		
		TrainDetailsVO traindetailsobj = new TrainDetailsVO();
		String[] input = line.split("\\|");
		
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Date date1 = new Date();
		Date travelDate1 = new SimpleDateFormat("dd-MM-yyyy").parse(travelDate);
		System.out.println("curn date"+date1+"traveldate"+travelDate1);
		
		String regex = "[0-9]+";
		String pattern = "^[a-zA-Z0-9]*$";//for alphanumeric
		
		if(!regex.matches(input[2]) && "0".equals(input[2].indexOf(0))){
			throw new TrainServiceException("Source should be no");
		}
		if(!regex.matches(input[3]) && "0".equals(input[3].indexOf(0))){
			throw new TrainServiceException("Destination should be no");
		}
		if(input[2]==input[3]){
			throw new TrainServiceException("Source and Destinatio no can not be same");
		}
		if(date1.getTime() > travelDate1.getTime())
		{
			throw new TrainServiceException("Travel date should be greater than current date");
		}
		traindetailsobj.setTrainNumber(input[0]);
		traindetailsobj.setRoute(input[1]);
		traindetailsobj.setSource(Integer.parseInt(input[2]));
		traindetailsobj.setDestination(Integer.parseInt(input[3]));
		traindetailsobj.setSpecial(input[4].charAt(0));
		traindetailsobj.setDateOfTravel(convertStringtoDate(travelDate));
		return traindetailsobj;
	}


	private static Date convertStringtoDate(String travelDate2) throws TrainServiceException {
		Date date = null;
		try
		{
			date = new SimpleDateFormat("dd-MM-yyyy").parse(travelDate2);
		}catch(Exception e)
		{
			throw new TrainServiceException("Date should be dd-MM-yyyy format");
		}
		return date;
	}


	/* Return the special trains Map<Integer,TreeSet> */
	public static Map getTrainSchedule(String filePath) throws TrainServiceException {
		Map<Integer,TreeSet> finalMap = new HashMap<Integer,TreeSet>();
		TreeSet<Integer> ts;
		int key=1;
		try
		{
			ts = processFile(filePath);
			if(ts!=null)
			{
				finalMap.put(key, ts);
			}else{
				ts = new TreeSet<Integer>(); 
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		System.out.println("Train Schedule => "+finalMap.toString());
		return finalMap;
	}


	private static TreeSet<Integer> processFile(String filePath) throws TrainServiceException {
		
		TreeSet<Integer> ts = new TreeSet<Integer>();
		try
		{
			BufferedReader br =new BufferedReader(new FileReader(filePath));
			String line = null;
			while((line=br.readLine())!=null)
			{
				ts.add(fetchTrainNos(line));
			}
		}catch(Exception e)
		{
			//throw new TrainServiceException("Date should be dd-MM-yyyy format");	
		}
		return ts;
	}


	private static Integer fetchTrainNos(String line) {
		try
		{
			String[] input = line.split(Pattern.quote(SPLITTER));
			if(input[4]!=null && input[4].equals("Y"))
			{
				return Integer.parseInt(input[0]);	
			}	
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	

}

/* Train Detail Value Object - DO NOT CHANGE*/
class TrainDetailsVO {
	private String trainNumber;
	private String route;
	private int source;
	private int destination;
	private char special;
	private Date dateOfTravel;
	
	public String getTrainNumber() {
		return trainNumber;
	}

	public void setTrainNumber(final String trainNumber) {
		this.trainNumber = trainNumber;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(final String route) {
		this.route = route;
	}

	public int getSource() {
		return source;
	}

	public void setSource(final int source) {
		this.source = source;
	}

	public int getDestination() {
		return destination;
	}

	public void setDestination(final int destination) {
		this.destination = destination;
	}

	public char getSpecial() {
		return special;
	}

	public void setSpecial(final char status) {
		this.special = special;
	}

	public void setDateOfTravel(final Date dateOfTravel){
		this.dateOfTravel= dateOfTravel;
	}
	public Date getDateOfTravel(){
		return dateOfTravel;
	}
	   
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TrainDetailsVO other = (TrainDetailsVO) obj;
		
		if (trainNumber == null) {
			if (other.trainNumber != null) {
				return false;
			}
		} else if (!trainNumber.equals(other.trainNumber)) {
			return false;
		}
		if (route == null) {
			if (other.route != null) {
				return false;
			}
		} else if (!route.equals(other.route)) {
			return false;
		}
		if (special == ' ') {
			if (other.special != ' ') {
				return false;
			}
		} else if (special != other.special) {
			return false;
		}
		if (destination != other.destination) {
			return false;
		}
		if (source != other.source) {
			return false;
		}
	
		return true;
	}

	
	@Override
	public String toString() {
		return "TrainDetailsVO [trainNo=" + trainNumber + ", route=" + route
				+ ", source=" + source + ", destination=" + destination+", dateOfTravel="+dateOfTravel
				+ "]";
		
	}

}

/* User defined Exception - DO NOT CHANGE */
class TrainServiceException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TrainServiceException(String message) {
		super(message);
	}

	public TrainServiceException(Throwable throwable) {
		super(throwable);
	}
}
