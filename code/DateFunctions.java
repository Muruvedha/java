package com.domain.demo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;


public class DateFunctions {

	public static void main(String[] args) {
		try
		{
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String start="30-12-2013";
		String end = "30-11-2016";
		String addm="12"; String months = "48";
		
		Date date1= sdf.parse(start);
		Date date2=sdf.parse(end);
		
		/*Calendar startDate = Calendar.getInstance();
		startDate.setTime(date1);
		Calendar endDate = Calendar.getInstance();
		endDate.setTime(date2);
		
		int diffyears = endDate.get(Calendar.YEAR) -  startDate.get(Calendar.YEAR);
		int diffmonths= diffyears*12+endDate.get(Calendar.MONTH) -  startDate.get(Calendar.MONTH);
		
		startDate.add(Calendar.MONTH, Integer.parseInt(addm));
		System.out.println(startDate.getTime());
		System.out.println(startDate.get(Calendar.DAY_OF_WEEK));
	
		SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
		String inputString1 = "23 01 1986";
		String inputString2 = "14 11 2016";
		

		try {*/
		   // Date date1 = myFormat.parse(inputString1);
		  //  Date date2 = myFormat.parse(inputString2);
		    long diff = date2.getTime() - date1.getTime();
		    System.out.println ("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
		   
		    Calendar startCalendar = Calendar.getInstance();
		    startCalendar.setTime(date1);
		    Calendar endCalendar =  Calendar.getInstance();
		    endCalendar.setTime(date2);

		    int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
		    System.out.println ("years: " + diffYear);
		    int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
		    //endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
		    System.out.println ("Months: " + diffMonth);
		    
		    startCalendar.add(Calendar.MONTH, Integer.parseInt(months));
		    System.out.println ("Adding Months: " + startCalendar.getTime());
		    
		    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		 //Date today = Calendar.getInstance().getTime();        
		 //String reportDate = df.format(startCalendar.getTime());
		 String reportDate = convertDatetoString(startCalendar.getTime());
		 System.out.println("Report Date: " + reportDate);
		    
		    System.out.println ("Day of the week: " + endCalendar.get(Calendar.DAY_OF_WEEK));
		    
		    
		} catch (ParseException e) {
		    e.printStackTrace();
		}

	}
	public static String convertDatetoString(Date date)
	{
		SimpleDateFormat date1 = new SimpleDateFormat("yyyy-MM-dd");
		String date12 = date1.format(date);
		return date12;
		
	}

}

