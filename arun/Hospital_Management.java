import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.swing.undo.CannotRedoException;

public class HospitalManagementDetails {

	public static void main(String[] args) throws PatientDetailIOException {
		
		File inputFile = new File("Z:/Documents/input.txt");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		if(inputFile.exists())
		{
			FileReader freader;
			try 
			{
				freader = new FileReader(inputFile);
				BufferedReader buffreader = new BufferedReader(freader);
				Map<String, List<PatientDetailVO>> categoryPatientMap = new HashMap<>();
				Map<String, Integer> patientCategoryMap = new HashMap<>();
				HashMap<Integer, Map> mainMap = new HashMap<>();
				
				List<PatientDetailVO> patientDetailsList = new ArrayList<>();
				Set<Date> st = new HashSet<>();
				
				int generalCount = 0;
				int entCount = 0;
				int neuCount = 0;
				
				String rowcontent = null;
				try 
				{
					
					
					while((rowcontent = buffreader.readLine()) != null)
					{
						PatientDetailVO patientVO = new PatientDetailVO();
						String[] record = rowcontent.split(";");
						validateRecord(record);
						patientVO.setPatientName(record[0]);
						patientVO.setMRN(record[1]);
						patientVO.setGender(record[2]);
						patientVO.setPhysicianID(record[3]);
						
						Date adDate = sdf.parse(record[4]);
						Date disDate = sdf.parse(record[5]);
						
						patientVO.setAdmissionDate(adDate);
						patientVO.setDischargeDate(disDate);
				
						st.add(adDate);
						
						long dateDiff = disDate.getTime() - adDate.getTime();
						long days = dateDiff / (24*60*60*1000);
						
						if(record[3].endsWith("GEN"))
						{
							int bill = (int) (days * 1200);
							patientVO.setBill(bill);
							generalCount++;
						}
						else if (record[3].endsWith("ENT")) 
						{
							int bill = (int) (days * 1600);
							patientVO.setBill(bill);
							entCount++;
						}
						else if(record[3].endsWith("NEU"))
						{
							int bill = (int) (days * 2000);
							patientVO.setBill(bill);
							neuCount++;
						}
						
						patientDetailsList.add(patientVO);
						
					}
					//System.out.println(patientDetailsList);
					List<Date> nonDupList = new ArrayList<Date>();
					Iterator<Date> iterator = st.iterator();
					
					while(iterator.hasNext())
						{
							nonDupList.add(iterator.next());
						}
					
					String nonDupDate = null;
					String voDate = null;
					for (int i = 0; i < nonDupList.size(); i++)
					{
						
						List<PatientDetailVO> finallist = new ArrayList<>();
						for (int j = 0; j < patientDetailsList.size(); j++) 
						{
							nonDupDate = sdf.format(nonDupList.get(i));
							PatientDetailVO vo = patientDetailsList.get(j);
							voDate = sdf.format(vo.getAdmissionDate());
							
							if(nonDupDate.equalsIgnoreCase(voDate))
							{
								//System.out.println(voDate);
								finallist.add(vo);
							}
						}
						categoryPatientMap.put(nonDupDate, finallist);
					}
					
					System.out.println("***Group patinent details based on Admission Date***");
					for(Map.Entry<String, List<PatientDetailVO>> entry : categoryPatientMap.entrySet())
					{
						System.out.println("Key  :  "+entry.getKey());
						List<PatientDetailVO> list = entry.getValue();
						System.out.println("Value :");
						for(PatientDetailVO vo : list)
						{
							System.out.println(vo.toString());
						}
					}
					
					patientCategoryMap.put("GEN", generalCount);
					patientCategoryMap.put("ENT", entCount);
					patientCategoryMap.put("NEU", neuCount);
					
					System.out.println("***Patient details based on catetory***");
					
					for(Map.Entry<String, Integer> entry : patientCategoryMap.entrySet())
					{
						System.out.println("Key  : "+entry.getKey());
						System.out.println("Value  : "+entry.getValue());
					}
					
					mainMap.put(1, categoryPatientMap);
					mainMap.put(2, patientCategoryMap);
					
					System.out.println("***Final Map***");
					
					for(Map.Entry<Integer, Map> entry : mainMap.entrySet())
					{
						System.out.println("Key  : "+entry.getKey()+" Value  :"+entry.getValue());
					}
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				} 
				catch (ParseException e) 
				{
					e.printStackTrace();
				}
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			}		
			
		}	

	}
	
	private static void validateRecord(String[] record) throws PatientDetailIOException 
	{
		boolean isError = false;
		boolean isValidDate = true;
		if(record.length == 6)
		{
			if(!((record[0].trim().length()) > 0))
			{
				isError = true;
				throw new PatientDetailIOException("Patient Name should not be empty");
			}
			if(!((record[1].trim().length()) > 0))
			{
				isError = true;
				throw new PatientDetailIOException("MRN should not be empty");
			}
			if(!((record[2].trim().length()) > 0))
			{
				isError = true;
				throw new PatientDetailIOException("Gender should not be empty");
			}
			if(!((record[3].trim().length()) > 0))
			{
				isError = true;
				throw new PatientDetailIOException("Physian ID should not be empty");
			}
			if(!((record[4].trim().length()) > 0))
			{
				isError = true;
				throw new PatientDetailIOException("Admission Date should not be empty");
			}
			if(!((record[5].trim().length()) > 0))
			{
				isError = true;
				throw new PatientDetailIOException("Discharge Date should not be empty");
			}
		}
		else
		{
			isError = true;
			throw new PatientDetailIOException("All fields are mandatory");
		}
		
		if(!isError)
		{
			if(!Pattern.matches("^[a-zA-Z\\s]*$", record[0]))
			{
				throw new PatientDetailIOException("Name Mismatch");
			}
			
			if(!Pattern.matches("^(IN|OUT)\\d*$", record[1]))
			{
				throw new PatientDetailIOException("MRN Mismatch");
			}
			
			if(!Pattern.matches("^\\d{5}-(ENT|GEN|NEU)$", record[3]))
			{
				throw new PatientDetailIOException("Physician ID mismatch");
			}
			
			if(record[4]!=null && record[5]!=null)
			{
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				try
				{
					Date adDate  = sdf.parse(record[4]);
					Date disDate = sdf.parse(record[5]);
				} 
				catch (ParseException e)
				{
					isValidDate = false;
					e.printStackTrace();
				}
			}
			
			if(isValidDate)
			{
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				
				try 
				{
					Date addDate = sdf.parse(record[4]);
					Date disDate = sdf.parse(record[5]);
					//System.out.println(addDate);
					if(addDate.after(disDate))
					{
						throw new PatientDetailIOException("Addmission date should not be greater that discharge date");
					}
				} 
				catch (ParseException e)
				{
					e.printStackTrace();
				}
			}
		}
		
	}

}

class PatientDetailVO {
	private String patientName;
	private String MRN;
	private String gender;
	private String physicianID;
	private Date admissionDate;
	private Date dischargeDate;
	private int bill;
	
	public int getBill() {
		return bill;
	}

	public void setBill(int bill) {
		this.bill = bill;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getMRN() {
		return MRN;
	}

	public void setMRN(String mRN) {
		MRN = mRN;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPhysicianID() {
		return physicianID;
	}

	public void setPhysicianID(String physicianID) {
		this.physicianID = physicianID;
	}

	public Date getAdmissionDate() {
		return admissionDate;
	}

	public void setAdmissionDate(Date admissionDate) {
		this.admissionDate = admissionDate;
	}

	public Date getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}
	
	public String toString()
    {
        return "PatientDetailVO[Patient Name: " + patientName + " Bill: " + bill + " MRN: " + MRN + " Gender: " + gender + "PhysicianID: " + physicianID + "Admission Date: " + admissionDate + "DischargeDate: " + dischargeDate+"]";
    }

}

class PatientDetailIOException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PatientDetailIOException(String message)
	{
		super(message);
	}
	public PatientDetailIOException(Throwable message)
	{
		super(message);
	}
	
}