package com.domain.demo;

// Do not include any package definition 
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyBankDeposit {
	
	public static void main(String[] args)
	{ 
		try
		{   //"D://corejava/loanDetails.txt"
			//C:\Users\566477\Desktop\oanDetails.txt
			//C://Users/566477/Desktop/loanDetails.txt
			Scanner reader = new Scanner(System.in);
			System.out.println("Enter file path");
			String filePath = reader.next();			
			//String filePath = args[0];
			processBankDepositData(filePath);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static Map<String, List<ParentAccountVO>> processBankDepositData(
			String filePath) throws BankOrganizerException, FileNotFoundException {
		
		Map<String, List<ParentAccountVO>> bankDepMap = new HashMap<String, List<ParentAccountVO>>();
		List<ParentAccountVO> parentAccountVOList = new ArrayList<ParentAccountVO>();
		try
		{
			parentAccountVOList=readFile(filePath);
			for(ParentAccountVO s : parentAccountVOList)
			{
				if("SAV".equals(s.getAccType()))
				{
					populateMaps(bankDepMap,"SAV",s);
				}else if("WM".equals(s.getAccType()))
				{
					populateMaps(bankDepMap,"WM",s);
				}else if("NRI".equals(s.getAccType()))
				{
					populateMaps(bankDepMap,"NRI",s);
				}
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		System.out.println(bankDepMap.toString());
		return bankDepMap;
		
	}
	
	private static void populateMaps(Map<String, List<ParentAccountVO>> bankDepMap, String key,
			ParentAccountVO parentAccountVOList) {
		if(bankDepMap!=null)
		{
			List<ParentAccountVO> polist = bankDepMap.get(key); 
			if(polist==null)
			{
				polist = new ArrayList<ParentAccountVO>();
			}
			polist.add(parentAccountVOList);
			bankDepMap.put(key, polist);
		}
		
	}

	private static List<ParentAccountVO> readFile(String filePath) {
		List<ParentAccountVO> parentAccountvo = new ArrayList<ParentAccountVO>();
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(filePath));
			String line = null;
			while((line=br.readLine())!=null)
			{
				parentAccountvo.add(convert(line));
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return parentAccountvo;
	}

	private static ParentAccountVO convert(String line) throws BankOrganizerException {
		//List<ParentAccountVO> parentAccVo = new ArrayList<ParentAccountVO>();
		
		ParentAccountVO po = new ParentAccountVO();
		String[] input = line.split(",");
		//Account type validation
		List<String> accountTypes = Arrays.asList("SAV","WM","NRI");
		if(!accountTypes.contains(input[2]))
		{
			throw new BankOrganizerException("Account types should be from WM, NRI, SAV");
		}
		
		LinkedDepositVO linkDepvobj = new LinkedDepositVO();
		List<LinkedDepositVO> listDepVO = new ArrayList<LinkedDepositVO>();
		
		linkDepvobj.setDepositAmount(Integer.parseInt(input[4]));
		linkDepvobj.setDepositMaturityDate(stringToDate(input[6]));
		linkDepvobj.setDepositStartDate(stringToDate(input[5]));
		linkDepvobj.setLinkedDepositNo(input[3]);
		linkDepvobj.setMaturityAmount(calculateMaturityAmount(stringToDate(input[6]),stringToDate(input[5]),Integer.parseInt(input[4])));
		
		listDepVO.add(linkDepvobj);
		
		po.setName(input[1]);
		po.setAccType(input[2]);
		po.setLinkedDeposits(listDepVO);
		po.setParentAccNo(Integer.parseInt(input[0]));
		//parentAccVo.add(po);
		return po;
	}

	private static Date stringToDate(String string) {
		Date date = null;
		try
		{
			date = new SimpleDateFormat("dd-MM-yyyy").parse(string);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return date;
	}
	
	private static float calculateMaturityAmount(Date date1, Date date2,int depositamount){
		float maturity_amount=0.00f;
		long diff = date1.getTime() - date2.getTime();
		long duration = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		maturity_amount = depositamount+(depositamount * getRateofInterest(duration) / 100);
		return maturity_amount;
		
	}

	private static float getRateofInterest(long duration) {
		float roi = 0.00f;
		if(duration>=0 && duration<=200)
		{
			roi=6.75f;
		}else if(duration>=201 && duration<=400)
		{
			roi=7.5f;
		}else if(duration>=401 && duration<=600)
		{
			roi=8.75f;
		}else { roi=10f; }
		return roi;
	}
	
	/*public static boolean validateData(String[] str) {
		//write your code here

	}*/
	

}

class ParentAccountVO {

	private int parentAccNo;
	private String name;
	private String AccType;
	//private LinkedDepositVO linkedDeposit;
	private List<LinkedDepositVO> linkedDeposits;

	public int getParentAccNo() {
		return parentAccNo;
	}

	public void setParentAccNo(int parentAccNo) {
		this.parentAccNo = parentAccNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccType() {
		return AccType;
	}

	public void setAccType(String accType) {
		AccType = accType;
	}

	public List<LinkedDepositVO> getLinkedDeposits() {
		return linkedDeposits;
	}

	public void setLinkedDeposits(List<LinkedDepositVO> linkedDeposits) {
		this.linkedDeposits = linkedDeposits;
	}

	public boolean equals(Object object) {
		boolean isEqual = false;
		ParentAccountVO otherAccount = (ParentAccountVO) object;
		if ((this.parentAccNo == otherAccount.parentAccNo)
				&& (this.AccType.equals(otherAccount.getAccType()) && (this.linkedDeposits
						.equals(otherAccount.getLinkedDeposits())))) {
			isEqual = true;
		}
		return isEqual;
	}

	

	@Override
	public String toString() {
		return "ParentAccountVO [parentAccNo=" + parentAccNo + ", name=" + name
				+ ", AccType=" + AccType + ", linkedDeposits=" + linkedDeposits
				+ "]";
		
	//	return parentAccNo  + "  , " +  name  + " ," + AccType + " ," +  linkedDeposits;
		
	}

}

class LinkedDepositVO {

	private String linkedDepositNo;
	private int depositAmount;
	private Date depositStartDate;
	private Date depositMaturityDate;
	private float maturityAmount;

	public String getLinkedDepositNo() {
		return linkedDepositNo;
	}

	public void setLinkedDepositNo(String linkedDepositNo) {
		this.linkedDepositNo = linkedDepositNo;
	}

	public int getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(int depositAmount) {
		this.depositAmount = depositAmount;
	}

	public Date getDepositStartDate() {
		return depositStartDate;
	}

	public void setDepositStartDate(Date depositStartDate) {
		this.depositStartDate = depositStartDate;
	}

	public Date getDepositMaturityDate() {
		return depositMaturityDate;
	}

	public void setDepositMaturityDate(Date depositMaturityDate) {
		this.depositMaturityDate = depositMaturityDate;
	}

	public float getMaturityAmount() {
		return maturityAmount;
	}

	public void setMaturityAmount(float maturityAmount) {
		this.maturityAmount = maturityAmount;
	}

	public boolean equals(Object object) {
		boolean isEquals = false;
		LinkedDepositVO depositVO = (LinkedDepositVO) object;
		if (this.linkedDepositNo.equals(depositVO.getLinkedDepositNo())
				&& (this.depositAmount == depositVO.getDepositAmount())
				&& (this.depositStartDate.equals(depositVO
						.getDepositStartDate()))
				&& (this.maturityAmount == depositVO.getMaturityAmount())) {
			isEquals = true;
		}
		return isEquals;
	}

	@Override
	public String toString() {
	
	
		return "LinkedDepositVO [linkedDepositNo=" + linkedDepositNo
				+ ", depositAmount=" + depositAmount + ", depositStartDate="
				+ depositStartDate + ", depositMaturityDate="
				+ depositMaturityDate + ", maturityAmount=" + maturityAmount
				+ "]"; 
		
	//	return linkedDepositNo  + "  , " +  depositAmount  + " ," + depositStartDate + " ," +  depositMaturityDate + "," + maturityAmount;
	}

}

class BankOrganizerException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BankOrganizerException(String message) {
		super(message);
	}

	public BankOrganizerException(Throwable throwable) {
		super(throwable);
	}

	public BankOrganizerException(String message, Throwable throwable) {
		super(message, throwable);
	}
}

/************************************************************/
/*
 * DO NOT CHANGE THE BELOW CLASS. THIS IS FOR VERIFYING THE CLASS NAME AND
 * METHOD SIGNATURE USING REFLECTION APIs
 */
/************************************************************/
class Validator {

	private static final Logger LOG = Logger.getLogger("Validator");

	public Validator(String filePath, String className, String methodWithExcptn) {
		validateStructure(filePath, className, methodWithExcptn);
	}

	protected final void validateStructure(String filePath, String className,
			String methodWithExcptn) {

		if (validateClassName(className)) {
			validateMethodSignature(methodWithExcptn, className);
		}

	}

	protected final boolean validateClassName(String className) {

		boolean iscorrect = false;
		try {
			Class.forName(className);
			iscorrect = true;
			LOG.info("Class Name is correct");

		} catch (ClassNotFoundException e) {
			LOG.log(Level.SEVERE, "You have changed either the "
					+ "class name/package. Use the default package "
					+ "and class name as provided in the skeleton");

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "There is an error in validating the "
					+ "Class Name. Please manually verify that the "
					+ "Class name is same as skeleton before uploading");
		}
		return iscorrect;

	}

	protected final void validateMethodSignature(String methodWithExcptn,
			String className) {
		Class cls;
		try {

			String[] actualmethods = methodWithExcptn.split(",");
			boolean errorFlag = false;
			String[] methodSignature;
			String methodName = null;
			String returnType = null;

			for (String singleMethod : actualmethods) {
				boolean foundMethod = false;
				methodSignature = singleMethod.split(":");

				methodName = methodSignature[0];
				returnType = methodSignature[1];
				cls = Class.forName(className);
				Method[] methods = cls.getMethods();
				for (Method findMethod : methods) {
					if (methodName.equals(findMethod.getName())) {
						foundMethod = true;
						if ((findMethod.getExceptionTypes().length != 1)) {
							LOG.log(Level.SEVERE, "You have added/removed "
									+ "Exception from '" + methodName
									+ "' method. "
									+ "Please stick to the skeleton provided");
						}
						if (!(findMethod.getReturnType().getName()
								.equals(returnType))) {
							errorFlag = true;
							LOG.log(Level.SEVERE, " You have changed the "
									+ "return type in '" + methodName
									+ "' method. Please stick to the "
									+ "skeleton provided");

						}

					}
				}
				if (!foundMethod) {
					errorFlag = true;
					LOG.log(Level.SEVERE,
							" Unable to find the given public method "
									+ methodName + ". Do not change the "
									+ "given public method name. "
									+ "Verify it with the skeleton");
				}

			}
			if (!errorFlag) {
				LOG.info("Method signature is valid");
			}

		} catch (Exception e) {
			LOG.log(Level.SEVERE,
					" There is an error in validating the "
							+ "method structure. Please manually verify that the "
							+ "Method signature is same as the skeleton before uploading");
		}
	}
}
