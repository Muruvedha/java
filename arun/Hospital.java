import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;


public class Hospital {

	public static void main(String[] args) 
	{
		File inputFile = new File("Z:/Documents/input.txt");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		if(inputFile.exists())
		{
			try 
			{
				FileReader fReader = new FileReader(inputFile);
				BufferedReader buffReader = new BufferedReader(fReader);
				String rowContent = null;
				int genCount = 0;
				int entCount = 0;
				int neuCount = 0;
				Map<String, List<PatientDetailVO1>> categoryPatientMap = new HashMap<>();
				Map<String, Integer> patientCategoryMap = new HashMap<>();
				HashMap<Integer, Map> finalMap = new HashMap<>();
				
				Set<Date> uniqueDate = new HashSet<>();
				List<PatientDetailVO1> patientList = new ArrayList<>();
				while((rowContent = buffReader.readLine()) != null)
				{
					PatientDetailVO1 detailsVO  = new PatientDetailVO1();
					String[] record = rowContent.split(";");
					validateRecord(record, detailsVO);
					long diff = detailsVO.getDischargeDate().getTime() - detailsVO.getAdmissionDate().getTime();
					long days = diff / (1000*60*60*24);
					
					if(detailsVO.getPhysicianID().endsWith("GEN"))
					{
						int bill = (int) (days * 1200);
						detailsVO.setBill(bill);
						genCount++;
					}
					else if(detailsVO.getPhysicianID().endsWith("ENT"))
					{
						int bill = (int) (days * 1600);
						detailsVO.setBill(bill);
						entCount++;
					}
					else if(detailsVO.getPhysicianID().endsWith("NEU"))
					{
						int bill = (int) (days * 2400);
						detailsVO.setBill(bill);
						neuCount++;
					}
					uniqueDate.add(detailsVO.getAdmissionDate());
					patientList.add(detailsVO);
				}
				fReader.close();
				buffReader.close();
				
				String setDate = null;
				String voDate = null;
				for (Date nonDupDate : uniqueDate)
				{
					List<PatientDetailVO1> categoryPatientList = new ArrayList<>();
					for (int i = 0; i < patientList.size(); i++)
					{
						setDate = sdf.format(nonDupDate);
						voDate = sdf.format(patientList.get(i).getAdmissionDate());
						
						if(setDate.equals(voDate))
						{
							categoryPatientList.add(patientList.get(i));
						}
					}
					categoryPatientMap.put(setDate, categoryPatientList);
				}
				
				System.out.println("***Group patinent details based on Admission Date***");
		
				for(Map.Entry<String, List<PatientDetailVO1>> entry : categoryPatientMap.entrySet())
				{
					System.out.println(entry.getKey());
					List<PatientDetailVO1> list = entry.getValue();
					for(PatientDetailVO1 vo : list)
					{
						System.out.println(vo.toString());
					}
				}
				
				patientCategoryMap.put("GEN", genCount);
				patientCategoryMap.put("ENT", entCount);
				patientCategoryMap.put("NEU", neuCount);
				
				System.out.println("***Patient details based on catetory***");
				for(Map.Entry<String, Integer> entry : patientCategoryMap.entrySet())
				{
					System.out.println("Key  : "+entry.getKey());
					System.out.println("Value  : "+entry.getValue());
				}
				
				finalMap.put(1, categoryPatientMap);
				finalMap.put(2, patientCategoryMap);

			}
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			} 
			catch (PatientDetailIOException1 e) 
			{
				e.printStackTrace();
			}

		}

	}

	private static void validateRecord(String[] record, PatientDetailVO1 detailsVO) throws PatientDetailIOException1 
	{
		boolean isError = false;
		boolean isValidDate = true;
		
		if(record.length == 6)
		{
			if(!(record[0].trim().length() > 0))
			{
				isError = true;
				throw new PatientDetailIOException1("Name mandatory");
			}
			if(!(record[1].trim().length() > 0))
			{
				isError = true;
				throw new PatientDetailIOException1("MRN mandatory");
			}
			if(!(record[2].trim().length() > 0))
			{
				isError = true;
				throw new PatientDetailIOException1("Gender mandatory");
			}
			if(!(record[3].trim().length() > 0))
			{
				isError = true;
				throw new PatientDetailIOException1("Physician Mandatory");
			}
			if(!(record[4].trim().length() > 0))
			{
				isError = true;
				throw new PatientDetailIOException1("Add date mandatory");
			}
			if(!(record[5].trim().length() > 0))
			{
				isError = true;
				throw new PatientDetailIOException1("discharge date mandatory");
			}
		}
		else
		{
			isError = true;
			throw new PatientDetailIOException1("All fields are Mandatory");
		}
		
		if(!isError)
		{
			if(Pattern.matches("^[a-zA-Z\\s]*$", record[0]))
			{
				detailsVO.setPatientName(record[0]);
			}
			else
			{
				throw new PatientDetailIOException1("Name mismatch");
			}
			
			if(Pattern.matches("^(IN|OUT)\\d*$", record[1]))
			{
				detailsVO.setMRN(record[1]);
			}
			else
			{
				throw new PatientDetailIOException1("MRN mismatch");
			}
			
			if(Pattern.matches("^\\d{5}-(GEN|ENT|NEU)$", record[3]))
			{
				detailsVO.setPhysicianID(record[3]);
			}
			else
			{
				throw new PatientDetailIOException1("Physician mismatch");
			}
			
			if(record[4]!=null && record[5]!=null)
			{
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				try 
				{
					Date adDate = sdf.parse(record[4]);
					Date disDate = sdf.parse(record[5]);
				} 
				catch (ParseException e) 
				{ 
					isValidDate = false;
					e.printStackTrace();
				}
			}
			
			if(isValidDate)
			{
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				try 
				{
					Date adDate = sdf.parse(record[4]);
					Date disDate = sdf.parse(record[5]);
					if(adDate.after(disDate))
					{
						throw new PatientDetailIOException1("Add date shoule not be greater that discharge date");
					}
					else
					{
						detailsVO.setAdmissionDate(adDate);
						detailsVO.setDischargeDate(disDate);
					}
				} 
				catch (ParseException e) 
				{ 
					e.printStackTrace();
				}
			}
			
		}
	}

}

class PatientDetailVO1 {
	private String patientName;
	private String MRN;
	private String gender;
	private String physicianID;
	private Date admissionDate;
	private Date dischargeDate;
	private int bill;
	
	public int getBill() {
		return bill;
	}

	public void setBill(int bill) {
		this.bill = bill;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getMRN() {
		return MRN;
	}

	public void setMRN(String mRN) {
		MRN = mRN;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPhysicianID() {
		return physicianID;
	}

	public void setPhysicianID(String physicianID) {
		this.physicianID = physicianID;
	}

	public Date getAdmissionDate() {
		return admissionDate;
	}

	public void setAdmissionDate(Date admissionDate) {
		this.admissionDate = admissionDate;
	}

	public Date getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}
	
	public String toString()
    {
        return "PatientDetailVO[Patient Name: " + patientName + " Bill: " + bill + " MRN: " + MRN + " Gender: " + gender + "PhysicianID: " + physicianID + "Admission Date: " + admissionDate + "DischargeDate: " + dischargeDate+"]";
    }

}

class PatientDetailIOException1 extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PatientDetailIOException1(String message)
	{
		super(message);
	}
	public PatientDetailIOException1(Throwable message)
	{
		super(message);
	}
	
}
