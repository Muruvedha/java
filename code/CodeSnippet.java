package com.domain.demo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.Calendar;

public class CodeSnippet {

	private static String SPLITTER = "|";

	public static void main(String[] args) {

		List<String> stringList = Arrays.asList(new String[] { "fra", "b", "a", "b", "c", "a" });
		System.out.println(findDuplicates(stringList));

	}


	private static <T> List<T> findDuplicates(List<T> orignalList) {

		List<T> dupes = new ArrayList<T>();
		Set<T> temp = new HashSet<T>();
		for (T s : orignalList) {
			if (!temp.add(s)) {
				dupes.add(s);
			}
		}
		return dupes;
	}

	private static List<PatientDetails> readFile(String filePath) {
		List<PatientDetails> fileList = new ArrayList<PatientDetails>();

		try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
			if (reader != null) {
				String line = null;
				while ((line = reader.readLine()) != null) {
					fileList.add(convert(line));
				}
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return fileList;
	}

	private static Map<String, List<PatientDetails>> byId = new HashMap<String, List<PatientDetails>>();
	private static Map<String, List<PatientDetails>> byGender = new HashMap<String, List<PatientDetails>>();

	private static void populateLookUps(List<PatientDetails> patientsList) {

		for (PatientDetails patient : patientsList) {
			putValues(byId, patient.getId(), patient);
			putValues(byGender, patient.getGender(), patient);
			//-- add more
		}
	}

	private static <K, V> void putValues(Map<K, List<V>> map, K key, V value) {
		List<V> tempList = null;
		if ((tempList = map.get(key)) == null) {
			tempList = new ArrayList<V>();
		}
		tempList.add(value);
		map.put(key, tempList);
	}

	private static PatientDetails convert(String line) {
		PatientDetails patientDetails = new PatientDetails();
		String[] split = line.split(Pattern.quote(SPLITTER));

		if (split != null && split.length > 0) {
			patientDetails.setName(split[0]);
			patientDetails.setId(split[1]);
			patientDetails.setGender(split[2]);
			patientDetails.setOutp(split[3]);
			patientDetails.setAdmission(convertStringToDate(split[4]));
			patientDetails.setDischarge(convertStringToDate(split[5]));
			patientDetails.setBill(Integer.parseInt(split[6]));
		}

		return patientDetails;
	}

	private static Date convertStringToDate(String dateString) {
		Date date = null;
		if (dateString != null && dateString.length() > 0) {
			try {
				date = new SimpleDateFormat("ddMMyyyy").parse(dateString);
			} catch (Exception e) {
				e.printStackTrace(System.out);
			}
		}
		return date;
	}

	static class PatientDetails {

		private String name;
		private String id;
		private String gender;
		private String outp;
		private Date admission;
		private Date discharge;
		private int bill;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

		public String getOutp() {
			return outp;
		}

		public void setOutp(String outp) {
			this.outp = outp;
		}

		public Date getAdmission() {
			return admission;
		}

		public void setAdmission(Date admission) {
			this.admission = admission;
		}

		public Date getDischarge() {
			return discharge;
		}

		public void setDischarge(Date discharge) {
			this.discharge = discharge;
		}

		public int getBill() {
			return bill;
		}

		public void setBill(int bill) {
			this.bill = bill;
		}

		@Override
		public String toString() {
			return "PatientDetails [Name=" + getName() + ", Id=" + getId() + ", Gender=" + getGender() + ", Outp="
					+ getOutp() + ", Admission=" + getAdmission() + ", Discharge=" + getDischarge() + ", Bill="
					+ getBill() + "]";
		}
		List<String> list = Arrays.asList(new String() {"a","b","a","c"});
		
		private <T> List<T> findDup(List<T> orgiList)
		{
			List<T> dupes = new ArrayList<T>();
			Set<T> temp = new Set<T>();
			
			for(T list1: orgiList)
			{
				if(!temp.add(list1))
					dupes.add(list1);
			}
			
		}
	}

}