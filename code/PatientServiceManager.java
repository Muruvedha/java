package com.domain.demo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;


public class PatientServiceManager {
	
	private static String SPLITTER="|";
	
	public static void main(String[] args)
	{
		String filePath="D://corejava/patient_details.txt";
		getPatientDetails(filePath);
	}
	
	private static void getPatientDetails(String filePath) {
		List<PatientDetails> patient_details = new ArrayList<PatientDetails>();
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(filePath));
			String line = null;
			while((line=br.readLine())!=null)
			{
				patient_details.add(readLine(line));	
			}
			System.out.println(patient_details.toString());
			populateLookUps(patient_details);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private static Map<String, List<PatientDetails>> byId = new HashMap<String, List<PatientDetails>>();
	private static Map<String, List<PatientDetails>> byGender = new HashMap<String, List<PatientDetails>>();

	private static void populateLookUps(List<PatientDetails> patient)
	{
		for(PatientDetails patientobj :  patient)
		{
			putValues(byId,patientobj.getId(), patientobj);
			putValues(byGender, patientobj.getGender(),patientobj);
		}
		System.out.println(byId.toString());
		System.out.println(byGender.toString());
	}
	
	private static <K,V> void putValues(Map<K, List<V>> map, K key, V value)
	{
		List<V> tempList = null;
		if((tempList = map.get(key))==null)
		{
			tempList = new ArrayList<V>();
		}
		tempList.add(value);
		map.put(key, tempList);
	}
	
	private static PatientDetails readLine(String line) {
		PatientDetails poobj = new PatientDetails();
		String[] split = line.split(Pattern.quote(SPLITTER));
		if(split!=null && split.length > 0)
		{
			poobj.setName(split[0]);
			poobj.setId(split[1]);
			poobj.setGender(split[2]);
			poobj.setOutp(split[3]);
			poobj.setAdmission(stringToDate(split[4]));
			poobj.setDischarge(stringToDate(split[5]));
			poobj.setBill(Integer.parseInt(split[6]));
		}
		return poobj;
	}

	private static Date stringToDate(String stringDate)
	{
		Date date = null;
		try{
			date = new SimpleDateFormat("dd-MM-yyyy").parse(stringDate);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return date;
		
	}
	
}
class PatientDetails {

	private String name;
	private String id;
	private String gender;
	private String outp;
	private Date admission;
	private Date discharge;
	private int bill;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getOutp() {
		return outp;
	}

	public void setOutp(String outp) {
		this.outp = outp;
	}

	public Date getAdmission() {
		return admission;
	}

	public void setAdmission(Date admission) {
		this.admission = admission;
	}

	public Date getDischarge() {
		return discharge;
	}

	public void setDischarge(Date discharge) {
		this.discharge = discharge;
	}

	public int getBill() {
		return bill;
	}

	public void setBill(int bill) {
		this.bill = bill;
	}

	@Override
	public String toString() {
		return "PatientDetails [Name=" + getName() + ", Id=" + getId() + ", Gender=" + getGender() + ", Outp="
				+ getOutp() + ", Admission=" + getAdmission() + ", Discharge=" + getDischarge() + ", Bill="
				+ getBill() + "]";
	}

}
