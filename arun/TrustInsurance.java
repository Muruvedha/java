import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.security.auth.login.AccountLockedException;
import javax.swing.plaf.synth.SynthSeparatorUI;

public class TrustInsurance {

	public static void main(String[] args) 
	{
		String filepath = "Z:/Documents/insurance.txt";
		String sanctionDate = "23/06/2013";
		
		TrustInsurance trust = new TrustInsurance();
		HashMap<Integer, Map> finalMap = trust.getMainmap(filepath, sanctionDate);

	}

	private HashMap<Integer, Map> getMainmap(String filepath, String sanctionDate)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		File inputFile = new File(filepath);
		
		if(inputFile.exists())
		{
			try 
			{
				FileReader freader = new FileReader(filepath);
				BufferedReader buffreader = new BufferedReader(freader);
				List<TrustInsuranceVO> recipientList = new ArrayList<>();
				List<TrustInsuranceVO> duplist = new ArrayList<>();
				List<String> loanlist = new ArrayList<>();
				List<TrustInsuranceVO> nonDupList = new ArrayList<>();
				Map<String, List<TrustInsuranceVO>> map1 = new HashMap<>();
				Map<String, List<TrustInsuranceVO>> map2 = new HashMap<>();
				HashMap<Integer, Map> mainMap = new HashMap<>();
				
				String rowcontent = null;
				
				try 
				{
					while((rowcontent = buffreader.readLine()) != null)
					{
						TrustInsuranceVO recipientVO = new TrustInsuranceVO();
						String[] record  = rowcontent.split(";");
						validateRecord(record);
						Date endDate = getPolicyEndDate(record[2], record[3]);
						long loanPeriod = getLoanPeriod(sanctionDate, endDate);
						//System.out.println("loan Period "+loanPeriod);					
						loanlist.add(record[5]);					
						recipientVO.setPolicyCode(record[0]);
						recipientVO.setPan(record[1]);
						recipientVO.setStartDate(sdf.parse(record[2]));
						recipientVO.setEndDate(endDate);
						recipientVO.setPeriod(Integer.parseInt(record[3]));
						long calPremium = getCalPremium(record[0], record[3]);
						long incresePremium = getIncreasePremium(record[5], loanPeriod);
						long netPremium = calPremium + incresePremium;
						//System.out.println("Net Premium "+netPremium);
						recipientVO.setAccumulatedPremium((long) Integer.parseInt(record[4]));
						recipientVO.setNetPremium(netPremium);
						recipientList.add(recipientVO);					
					}
					//System.out.println(recipientList);
					
					//iterate main list to find duplicate records
					List<String> validloan = new ArrayList<>();
					for (int i = 0; i < recipientList.size(); i++)
					{
						boolean isdup = false;
						for (int j = i+1; j < recipientList.size(); j++) 
						{
							if(recipientList.get(i).equals(recipientList.get(j)))
							{
								isdup = true;
								duplist.add(recipientList.get(j));
							}
						}
						
						if(!isdup)
						{
							nonDupList.add(recipientList.get(i));
							validloan.add(loanlist.get(i));
						}						
					}

					List<TrustInsuranceVO> eligibleList = new ArrayList<>();
					List<TrustInsuranceVO> nonEligibleList = new ArrayList<>();
					//iterete non duplicate list to find eligible and non eligible customers
					for (int i = 0; i < nonDupList.size(); i++)
					{
						boolean isEligible = getEligibleStatus(nonDupList.get(i), validloan.get(i));
						
						if(isEligible)
						{
							eligibleList.add(nonDupList.get(i));
						}
						else
						{
							nonEligibleList.add(nonDupList.get(i));
						}
						
					}
					
					//iterate the main list to fine invalid records
					List<TrustInsuranceVO> invalidList = new ArrayList<>();
					for (int i = 0; i <  recipientList.size(); i++)
					{
						boolean isinvalid = getInvalidRecords(recipientList.get(i), loanlist.get(i));
						if(isinvalid)
						{
							invalidList.add(recipientList.get(i));
						}
					}
					
					map1.put("ELG", eligibleList);
					map1.put("NELG", nonEligibleList);
					
					System.out.println("******** Display eligible and non eligible records ********");
					
					for(Map.Entry<String, List<TrustInsuranceVO>> entry : map1.entrySet())
					{
						System.out.println("Key   : "+entry.getKey());
						List<TrustInsuranceVO> list = entry.getValue();
						System.out.println("Value  :");
						for (TrustInsuranceVO trustInsuranceVO : list) {
							System.out.println(trustInsuranceVO.toString());
						}
					}
					
					map2.put("DUP", duplist);
					map2.put("INVALID", invalidList);
					
					System.out.println("******** Display duplicate and invalid records ******");
					
					for(Map.Entry<String, List<TrustInsuranceVO>> entry : map2.entrySet())
					{
						System.out.println("Key   : "+entry.getKey());
						List<TrustInsuranceVO> list = entry.getValue();
						System.out.println("Value  :");
						for (TrustInsuranceVO trustInsuranceVO : list) 
						{
							System.out.println(trustInsuranceVO.toString());
						}
					}
					
					mainMap.put(1, map1);
					mainMap.put(2, map2);
					
					System.out.println("******** Display MainMap ********");
					for(Map.Entry<Integer, Map> entry : mainMap.entrySet())
					{
						System.out.println("Key   : "+entry.getKey()+" Value   : "+entry.getValue());
					}
					
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				} 
				catch (TrustInsuranceIOException e) 
				{
					e.printStackTrace();
				} 
				catch (ParseException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			}
			
			
		}
		return null;
	}

	private boolean getInvalidRecords(TrustInsuranceVO trustInsuranceVO, String string) {
		boolean isinvalid = false;
		String[] data = trustInsuranceVO.getPolicyCode().split("-");
		long sumassured = (long) Integer.parseInt(data[2]);
		long loan = (long) Integer.parseInt(string);
		if(loan > sumassured)
		{
			isinvalid = true;
		}
		return isinvalid;
	}

	private boolean getEligibleStatus(TrustInsuranceVO trustInsuranceVO, String string) 
	{
		boolean isEligible = false;
		long loan = (long) Integer.parseInt(string);
		long accumPremium = trustInsuranceVO.getAccumulatedPremium();
		String[] data = trustInsuranceVO.getPolicyCode().split("-");
		String policycode = data[0];
		long sumassured = (long) Integer.parseInt(data[2]);
		//System.out.println("acc "+accumPremium);
		//System.out.println("loan "+loan);
		if(policycode.equalsIgnoreCase("NRM"))
		{
			//System.out.println("NRM");
			long percentage = loan * 40 / 100;
			//System.out.println("Per "+percentage);
			if(percentage > accumPremium)
			{
				isEligible = false;
				//System.out.println("Not Eligible");
			}
			else
			{
				isEligible = true;
				//System.out.println("Eligible");
			}
		}
		else
		{
			long percent = loan * 60 / 100;
			long percent1 = sumassured * 70 / 100;
			if(percent < accumPremium)
			{
				isEligible = true;
				//System.out.println("Eligible");
			}
			else if((percent > accumPremium) && (percent1 < sumassured))
			{
				isEligible = true;
				//System.out.println("Eligible 1");
			}
			else
			{
				isEligible = false;
				//System.out.println("Not Eligible");
			}
		}
		return isEligible;
	}

	private long getIncreasePremium(String string, long loanPeriod)
	{
		long loan = (long) Integer.parseInt(string);
		//System.out.println("Loan "+loan);
		long interest = (long) (loan * 1.2 * loanPeriod / (100));
		//System.out.println("Interest "+interest);
		long increasePremium = (loan + interest) / loanPeriod;
		//System.out.println(increasePremium);
		return increasePremium;
	}

	private long getCalPremium(String string, String period) 
	{
		String[] data = string.split("-");
		long sum = (long) Integer.parseInt(data[2]);
		long calpremium = sum * Integer.parseInt(period);
		//System.out.println(calpremium);
		return calpremium;
	}

	private long getLoanPeriod(String sanctionDate, Date endDate)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		long month = 0;
		try 
		{
			Date sancDate = sdf.parse(sanctionDate);
			long diff = endDate.getTime() - sancDate.getTime();
			long days = diff / (1000*60*60*24);
			month = days / 30;
			//System.out.println(sanctionDate);
			//System.out.println(month);
		} 
		catch (ParseException e)
		{
			e.printStackTrace();
		}
		
		return month;
	}

	private Date getPolicyEndDate(String string, String period) 
	{
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Date startdate;
		Date endDate = null;
		try 
		{
			startdate = sdf.parse(string);
			cal.setTime(startdate);
			cal.add(Calendar.MONTH, Integer.parseInt(period));
			endDate = cal.getTime();
			//System.out.println(sdf.format(cal.getTime()));
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
		}		
		return endDate;
	}

	private void validateRecord(String[] record) throws TrustInsuranceIOException 
	{
		boolean isError = false;
		
		if(record.length < 6)
		{
			isError = true;
			throw new TrustInsuranceIOException("All fields are Mandatory");			
		}
		else
		{
			if(!(record[0].trim().length() > 0))
			{
				isError = true;
				throw new TrustInsuranceIOException("Policy Code should not be empty");
			}
			
			if(!(record[1].trim().length() > 0))
			{
				isError = true;
				throw new TrustInsuranceIOException("PAN should not be empty");
			}
			
			if(!(record[2].trim().length() > 0))
			{
				isError = true;
				throw new TrustInsuranceIOException("Policy Start Date should not be empty");
			}
			
			if(!(record[3].trim().length() > 0))
			{
				isError = true;
				throw new TrustInsuranceIOException("Period should not be empty");
			}
			
			if(!(record[4].trim().length() > 0))
			{
				isError = true;
				throw new TrustInsuranceIOException("Accumulated Premium should not be empty");
			}
			
			if(!(record[5].trim().length() > 0))
			{
				isError = true;
				throw new TrustInsuranceIOException("Loan amount should not be empty");
			}
		}
		
		if(!isError)
		{
			if(!Pattern.matches("^(FR)\\d{3}", record[1]))
			{
				throw new TrustInsuranceIOException("Pan Number should starts with FR followed by 3 digits");
			}
			
			String[] values = record[0].split("-");
			//System.out.println(values.length);
			
			if(values.length == 3)
			{
				if(!Pattern.matches("^(FST|NRM)", values[0]))
				{
					throw new TrustInsuranceIOException("Policy Type should be FST or NRM");
				}
				
				if(!Pattern.matches("^\\d[1-9]*$", values[1]))
				{
					throw new TrustInsuranceIOException("Policy number should be a non zero number");
				}
				
				if(!Pattern.matches("^\\d[0-9]*$", values[2]))
				{
					throw new TrustInsuranceIOException("Sum Assured should be a number");
				}
				
				int sumassured = Integer.parseInt(values[2]);
				int accumPremium = Integer.parseInt(record[4]);
				
				if(accumPremium > sumassured)
				{
					throw new TrustInsuranceIOException("Accumulated premium should not be greater that sum assured");
				}
			}
			
			else
			{
				throw new TrustInsuranceIOException("Policy Code Format mismatches");
			}
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			try 
			{
				Date startdate = sdf.parse(record[2]);
			} 
			catch (ParseException e) 
			{
				e.printStackTrace();
			}	
		}	
	}

}

class TrustInsuranceVO
{
	private String policyCode;
	private String pan;
	private Date startDate;
	private Date endDate;
	private Integer period;
	private long accumulatedPremium;
	private long netPremium;
	
	public String getPolicyCode() {
		return policyCode;
	}
	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getPeriod() {
		return period;
	}
	public void setPeriod(Integer period) {
		this.period = period;
	}
	public long getAccumulatedPremium() {
		return accumulatedPremium;
	}
	public void setAccumulatedPremium(long accumulatedPremium) {
		this.accumulatedPremium = accumulatedPremium;
	}
	public long getNetPremium() {
		return netPremium;
	}
	public void setNetPremium(long netPremium) {
		this.netPremium = netPremium;
	}
	
	@Override
	public String toString() 
	{
		return "TrustInsuranceVO [policyCode=" + policyCode + ", pan=" + pan + ", startDate=" + startDate + ", endDate="
				+ endDate + ", period=" + period + ", accumulatedPremium=" + accumulatedPremium + ", netPremium="
				+ netPremium + "]";
	}
	
	@Override
	public boolean equals(Object object) 
	{
		boolean isEqual = false;
		TrustInsuranceVO other = (TrustInsuranceVO) object;
		if((this.getPolicyCode().equals(other.getPolicyCode())) && (this.getStartDate().equals(other.getStartDate())) && (this.getAccumulatedPremium() == other.getAccumulatedPremium()))
				{
				isEqual = true;
				}
		return isEqual;
	}	
	
}

class TrustInsuranceIOException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public TrustInsuranceIOException(String message)
	{
		super(message);
	}
	public TrustInsuranceIOException(Throwable message)
	{
		super(message);
	}
	
}
