package com.domain.demo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.Comparator;

public class EmployeeServiceManager {
	
	private static String sourceFolder="C:/CCJP/files";
	private static String inputfileName = "EmployeeDetails.txt";

	
	public static void main(String[] args)
	{
		try
		{		
		EmployeeServiceManager emp = new EmployeeServiceManager();
		emp.processData(sourceFolder,inputfileName);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public Map<Integer, Map> processData(String sourceFolder,String inputfileName) throws EmployeeBusinessException {
		int key=1;
		String filePathString=sourceFolder+"/"+inputfileName;
		Map<Integer, Map> finalMap = new HashMap<Integer, Map>();
		Map<Integer,Object> empobjlist = new HashMap<Integer,Object>();
		try
		{
			empobjlist = readFile(filePathString);
			finalMap.put(key, empobjlist);
			System.out.println(finalMap.toString());
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return finalMap;
	}

	private Map<Integer, Object> readFile(String filePathString) throws EmployeeBusinessException {
		
		Map<Integer, Object> listofemp = new HashMap<Integer, Object>();
		File f = new File(filePathString);
		File foldercheck = new File("C:/CCJP/files");
		String filename= f.getName();
		String fileExtension = filename.substring(filename.lastIndexOf(".")+1);
		
		if(foldercheck != null && !foldercheck.isDirectory())
		{
		throw new EmployeeBusinessException("Folder does not exists");	
		}
		if(!f.exists() && f.isDirectory())
		{
			throw new EmployeeBusinessException("File is missing"); 
		}
		if(!fileExtension.equals("txt")){
			throw new EmployeeBusinessException("File is not in mentioned format");
		}
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(filePathString));
			String line = null;
			EmpVO empvoobj = new EmpVO();
			while((line=br.readLine())!=null)
			{
				String split[] = line.split(",");
				if(split!=null && split.length > 0)
				{
					empvoobj.setEmpId(split[0]);
					empvoobj.setEmpName(split[1]);
					empvoobj.setEmpDesignation(split[2]);
					listofemp.put(Integer.parseInt(split[0]), empvoobj.toString());
				}
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return listofemp;
	}


}
class EmployeeBusinessException extends Exception {
	public EmployeeBusinessException(String message) {
		super(message);
	}

	public EmployeeBusinessException(Throwable throwable) {
		super(throwable);
	}

}

class EmpVO {

	private String empId;
	private String empName;
	private String empDesignation;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpDesignation() {
		return empDesignation;
	}

	public void setEmpDesignation(String empDesignation) {
		this.empDesignation = empDesignation;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmpVO other = (EmpVO) obj;
		if (empId == null) {
			if (other.empId != null)
				return false;
		} else if (!empId.equals(other.empId))
			return false;
		if (empName == null) {
			if (other.empName != null)
				return false;
		} else if (!empName.equals(other.empName))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "{" + getEmpName() + "," + getEmpId() + ","+getEmpDesignation()+ "}";
	}
}

